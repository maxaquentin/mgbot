import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios               from 'axios'
import VueAxios            from 'vue-axios'
import './registerServiceWorker'
import 'ant-design-vue/dist/antd.css'
import {LocaleProvider, Input, Button } from 'ant-design-vue'

Vue.use(VueAxios, axios)
Vue.use(Button)
Vue.use(LocaleProvider)
Vue.use(Input)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
