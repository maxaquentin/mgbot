import Vue from 'vue'
import Router from 'vue-router'
// import Login from './views/Login.vue'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "about" */ './views/Home.vue'),
      // component: Home,
      meta: { 
        requiresAuth: true,
      }
    },
    {
      path: '/firms',
      component: () => import(/* webpackChunkName: "about" */ './views/Firms.vue'),
      // component: Home,
      meta: { 
        requiresAuth: true,
      }
    },
    {
      path: '/contract/:id',
      component: () => import(/* webpackChunkName: "about" */ './views/Contract.vue'),
      // component: Home,
      meta: { 
        requiresAuth: true,
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "about" */ './views/Login.vue')
    },
  ]
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
      if (localStorage.getItem('bot-jwt-auth') == null ){
          next({
              path: '/login',
              params: { nextUrl: to.fullPath }
          })
      }else{
          let user = JSON.parse(localStorage.getItem('bot-jwt-user'))
          if(to.matched.some(record => record.meta.is_admin)) {
              
          }else {
              next()
          }
      }
  } else if(to.matched.some(record => record.meta.guest)) {
      if(localStorage.getItem('jwt') == null){
          next()
      }
      else{
          next({ name: 'sms-receive'})
      }
  }else {
      next() 
  }
})

export default router;