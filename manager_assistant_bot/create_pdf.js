let pdf = require('pdf-lib')
let PDFDocumentFactory = pdf.PDFDocumentFactory
let PDFDocumentWriter = pdf.PDFDocumentWriter
let StandardFonts = pdf.StandardFonts
let drawText  = pdf.drawText
const  fs = require('fs');
const existingPdfDocBytes = fs.readFileSync('./daryo-banner-pr.pdf')
const pdfDoc = PDFDocumentFactory.load(existingPdfDocBytes);
const [helveticaRef, helveticaFont] = pdfDoc.embedStandardFont(
  StandardFonts.Helvetica,
);
 

const pages = pdfDoc.getPages();
const page  = pages[0];
const page2  = pages[3];
 
page.addFontDictionary('Helvetica', helveticaRef);
 
const contentStream = pdfDoc.createContentStream(
  drawText(helveticaFont.encodeText('Я тут'), {
    x: 100,
    y: 455,
    size: 10,
    font: 'Helvetica',
    colorRgb: [0, 0, 0],
  }),
);
 
page.addContentStreams(pdfDoc.register(contentStream));
page4.addContentStreams(pdfDoc.register(contentStream));

const pdfBytes = PDFDocumentWriter.saveToBytes(pdfDoc);
fs.writeFile('final.pdf', pdfBytes, (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
});
