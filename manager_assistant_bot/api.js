const express    = require('express');
const edit_pdf   = require('./testhtmlpdf.js')
const connection = require('./mongo_connect');
const bodyParser = require('body-parser');
const jwt        = require('jsonwebtoken');
const ObjectID   = require('mongodb').ObjectID;
const Telegram   = require('telegraf/telegram')
const helper_bot_token = '825306747:AAG3is7PWMBL1elZdNDTgNDadYJ1OZBL0WI';

let bot_options  = {
  agent: null,        
  webhookReply: true 
}

const telegram   = new Telegram(helper_bot_token, bot_options)
let   router     = express.Router();
let   dbname     = 'mg_bot_test'
let   db         = null;
router.use(async (req, res, next) => {
  db =  await new connection;
  next();
});

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

const allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', '*');
    next();
}

router.use(allowCrossDomain)

router.post('/login', async(req, res) => {
  try { 
    console.log(req.body)
    if(req.body.login == '123123' && req.body.login == '123123'){
      let user = {admin: 1, root:true}
      let token = 'fui89qwdj021iwoejcsijw9vub89UBF93R2U9FWN'
      res.send({user: token, user:user})
    } else{
      res.status(403).send('Invalid credentials');
    }
  } catch(err) {
    throw err;
  }
});

router.post('/edit/contract/:id', async (req, res) => {
  try {
    console.log(req.body);
    let dbase = await db.db(dbname);
    let categories = dbase.collection('contracts');
    let firms = dbase.collection('firms');
    delete req.body._id
    let latest   = await categories.find({executor_id:1}).limit(1).sort({$natural:-1}).toArray()
    // let diff     =  req.body.start_date - latest[0].start_date 
    // if(diff > (24*60*60*1000))
    // let exists   = await categories.findOne({start_date: req.body.start_date})
    let prev   = await categories.findOne({n: req.body.n-1})
    let next   = await categories.findOne({n: req.body.n+1})
    let sendBool = false;
    if(next & prev) {
      if(next.start_date > req.body.start_date && prev.start_date <req.body.start_date){
        sendBool = true
      }
    } else if(prev){
      if(prev.start_date < req.body.start_date){
        sendBool= true
      }
    } else if(next){
      if(next.start_date > req.body.start_date){
        sendBool = true
      }
    }
    if(!sendBool){
      res.send({errCode:13, date: latest[0].start_date})
    } else {
    console.log(req.body.broadcast_type)
    let data = {
      date: req.body.real_date,
      n: req.body.n,
      entity: req.body.entity,
      broadcast_type: req.body.broadcast_type,
    }
    if(req.body.license) data.license = req.body.license.filename
    // let firm = await firms.findOne({name: req.body.entity, executor_id: req.body.executor_id})
    let props = {}
    if(req.body.firm)  props = req.body.firm.props
    let newPdf = await edit_pdf(req.body.executor_id, data, props)
    req.body.document_path = await newPdf
    console.log(req.body.document_path)
    let editContract = await categories.updateOne(
    {_id: ObjectID(req.params.id)},
    {
      $set :req.body
    });
    res.send(await editContract);
  }
  } catch (err){
    res.status(500).send('Internal error');
    throw err;
  }
});    

router.post('/lock/contract/:id', async (req, res)=> {
  try {
    let dbase        = await db.db(dbname);
    let contracts    = dbase.collection('contracts')
    let contract     = await contracts.updateOne(
      {_id: ObjectID(req.params.id)},{
      $set: {
        locked: true
      }
    })
    res.send(contract)
  } catch(err) {
    res.status(500).send(err)
    throw err
  } 
}) 

router.post('/change/company/:id',async (req, res) => {
    try{
      let dbase        = await db.db(dbname);
      let contracts    = dbase.collection('contracts')
      let firms        = dbase.collection('firms')
      if(req.body.props.address == '') req.body.props.address = ' '
      if(req.body.props.address2 == '') req.body.props.address2 = ' '
      if(req.body.props.director == '') req.body.props.director = ' '
      if(req.body.props.phone == '') req.body.props.phone = ' '
      if(req.body.props.account == '') req.body.props.account = ' '
      if(req.body.props.bank == '') req.body.props.bank = ' '
      if(req.body.props.mfo == '') req.body.props.mfo = ' '
      if(req.body.props.inn == '') req.body.props.inn = ' '
      await contracts.updateOne({_id: ObjectID(req.params.id)}, {
        $set: {
          'firm.props': {...req.body.props}
        }
      })
      let firm         = await firms.updateOne({name: req.body.entity,executor_id:req.body.executor_id},{
        $set: {
          props: req.body.props
        }
      })
      let dog = await contracts.findOne({_id: ObjectID(req.params.id)}) 
      let data =  {
        date: dog.real_date,
        n: dog.n,
        entity: dog.entity,
        broadcast_type: dog.broadcast_type,
      } 
      await edit_pdf(req.body.executor_id, data,req.body.props)
      res.send(firm)
    }catch (err) {
      res.status(500).send('INTERNAL '+ err)
      throw err;
    }
})

router.post('/pay/:id', async (req, res) => {
    try {
      let dbase        = await db.db(dbname);
      let contracts    = dbase.collection('contracts');
      let editContract = await contracts.updateOne(
        {_id: ObjectID(req.params.id)},
        {
          $set : {
            ...req.body
          }
        })
        res.send(editContract)
        console.log(editContract)
    }catch (err){
      console.log(err)
    }
})

router.post('/accomplish/:id', async (req, res) => {
  try {
    console.log(req.body);
    let dbase        = await db.db(dbname);
    let contracts    = dbase.collection('contracts');
    let users         = dbase.collection('users');
    let accountants  = await users.find({type: 'accountant'}).toArray();
    let editContract = await contracts.updateOne(
      {_id: ObjectID(req.params.id)},
    {
      $set : {
        ...req.body.change
      }
    },
    );
    if(req.body.send){
      for(var i=0;i<accountants.length;i++){
        telegram.sendMessage(accountants[i].chat_id, req.body.executor+'\n'+req.body.entity+
        '\nБрэнд: '+req.body.brand_name+
        '\nКомпания: '+req.body.company_name+
        '\nОбработано (только что): '+req.body.done+
        '\nОбработано (итого): '+req.body.total_done+
        '\nОстаток: '+req.body.change.ad_left)
      }
    }
    res.send(await editContract);
    
  } catch (err){
    res.status(500).send('Internal error');
    throw err;
  }
});    

router.get('/get/contracts', async (req, res) => {
  try {
    let dbase = await db.db(dbname);
    let users = dbase.collection('contracts');
    let result = await users.find({}).toArray()
    res.send(result);
  } catch (err){
    res.status(500).send('Internal error');
    throw err;
  }
});


router.get('/get/firms', async (req, res) => {
  try {
    let dbase = await db.db(dbname);
    let users = dbase.collection('firms');
    let result = await users.find({}).toArray()
    res.send(result);
  } catch (err){
    res.status(500).send('Internal error');
    throw err;
  }
});

router.get('/get/contract/:id', async (req, res) => {
  try {
    let dbase = await db.db(dbname);
    let contracts = dbase.collection('contracts');
    let result = await contracts.findOne({_id: ObjectID(req.params.id.toString())})
    res.send(result);
  } catch (err){
    res.status(500).send('Internal error');
    throw err;
  }
});

module.exports = router;