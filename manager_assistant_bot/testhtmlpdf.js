var fs             = require('fs');
var pdf            = require('html-pdf');
const path         = require('path');
const numtoword    = require('./num_to_words');
// const util         = require('util');
// const readFile = util.promisify(fs.readFile);
var html_file = fs.readFileSync('./test.html', 'utf8');
var options = { format: 'Letter', 
quality: 99, 
    "border": {
    "top": "0.5in",            // default is 0, units: mm, cm, in, px
    "right": "0",
    "bottom": "0.8in",
    "left": ""
  } };
var html2json = require('html2json').html2json;
var json2html = require('html2json').json2html;

const dirs = [
    'Муслимовой Л. ',
    'Калимулина Б. ',
    'Агзамходжаева М. '
]


const exes         = ['YaTT “Muslimova Lola Alimbayevna”',
`ООО «Simple Networking Solutions»`,
'YaTT “Agzamxodjaev Muxrimxon Masudovich”'
]

const   recs  = [   `YaTT “Muslimova Lola Alimbayevna”\n
Адрес: г. Ташкент, Мирзо Улугбекский район,\n
ул. Паркент, д.24, кв.67\n
Тел.: +99893 533-74-33\n
Р/с: 2021 8000 0008 3931 9001\n
Мирзо Улугбекский ф-л АКБ «Капиталбанк»\n
МФО: 01018  \n
ИНН:482 910 279\n   
`,
`ООО «Simple Networking Solutions»\n
Адрес: г. Ташкент, Мирзо Улугбекский район,\n 
проспект Мустакиллик, дом 59А\n
Тел/факс: 140-86-85\n
Р/с: 2020 8000 2049 0302 8001\n
Чиланзарский ф-л ЧАК «Давр-банк»\n
МФО: 01046  ОКЭД: 58130\n
ИНН: 301 878 322   \n
`,
`YaTT “Agzamxodjaev Muxrimxon Masudovich”\n
Адрес: г. Ташкент, Чиланзарский район, \n
5-й проезд  Лутфий,  д.7\n
Тел.: +99890 930-60-62\n
Р/с: 2021 8000 9008 3919 7001\n
Чиланзарский ф-л ЧАКБ «Давр-банк»\n
МФО: 01046  \n
ИНН: 503 405 239\n   
`
]


const getHtml = async  (exec, data, props) => {
    data.sum = 3200000 
    console.log(data)
    var form_resolve = 'Устава'
    var form_resolve_m = 'Патента'
    if(exec == 2){
     form_resolve_m = 'Устава'
    }
    if(data.form == 'ООО' || data.form == 'ЧП' || data.form == 'СП'){
        form_resolve = 'Устава'
    } else if(data.form == 'ЯТТ' || data.form == 'ИП'){
        form_resolve = 'Патента'
    }

    console.log(form_resolve)
    console.log(form_resolve_m)
    let isp = exes[exec-1]
    let text_initial = ''


    text_initial = text_initial+isp+`, именуемое далее «Исполнитель», в лице директора `+dirs[exec-1]+`,
    действующее на основании `+form_resolve+`, с одной стороны, и `+data.entity+`,

    далее именуемое «Заказчик», в лице `+props.director+`, действующее на основании `+form_resolve_m+`, 
    с другой стороны, в дальнейшем именуемые Стороны, заключили настоящий договор о нижеследующем:`

    let text2 = `3.1.         Общая стоимость Услуг по Договору составляет `+numtoword(data.sum)+`\n
    сум, 00 тийин, в том числе НДС 7% `+numtoword(data.sum)+` \n
    сум, 00 тийин, которая установлена Сторонами с их согласия. Данный пункт Договора заменяет Протокол\n
    согласования договорной цены. Стоимость оказываемых Услуг по Договору считается фиксированной и не\n
    подлежит изменению в течение всего срока действия Договора \n`
        let json = html2json(html_file.toString())
        json.child[3].child.map(item => {
            if(item.attr){
                if(item.attr.id == 'docnum') item.child[0].text += data.n 
                if(item.attr.id == 'date') item.child[0].child[0].text = data.date
                if(item.attr.id == 'intro') item.child[0].text = text_initial
                if(item.attr.id == 'first-ol'){
                    item.child.map(i => {
                        if(i.tag == 'li'){
                            i.child.map(ol => {
                                if(ol.tag == 'ol'){
                                    ol.child.map(table_li => {
                                        if(table_li.tag == 'table'){
                                            let number = 0;
                                            for(let i=0;i<data.broadcast_type.length;i++){
                                                if(data.broadcast_type[i].types){
                                                    for(let j=0;j<data.broadcast_type[i].types.length;j++){
                                                        number = number + 1
                                                        table_li.child.push({
                                                            node:"element",
                                                            tag: 'tr',
                                                            child: [
                                                                {node:'element', tag:'td', child: [{node:'text', text: number }]},
                                                                {node:'element', tag:'td', child: [{node:'text', text: "Daryo" }]},
                                                                {node:'element', tag:'td', child: [{node:'text', text: data.broadcast_type[i].types[j].name }]},
                                                                {node:'element', tag:'td', child: [{node:'text', text: data.broadcast_type[i].types[j].q }]},
                                                                {node:'element', tag:'td', child: [{node:'text', text: data.broadcast_type[i].types[j].q*1400 }]},
                                                                {node:'element', tag:'td', child: [{node:'text', text: data.broadcast_type[i].types[j].q*1400+200 }]},
                                                                {node:'element', tag:'td', child: [{node:'text', text: ' '}]},
                                                            ]
                                                        })
                                                    }
                                                } else {
                                                    number += 1
                                                    table_li.child.push({
                                                        node:"element",
                                                        tag: 'tr',
                                                        child: [
                                                            {node:'element', tag:'td', child: [{node:'text', text:number }]},
                                                            {node:'element', tag:'td', child: [{node:'text', text: "Daryo" }]},
                                                            {node:'element', tag:'td', child: [{node:'text', text:data.broadcast_type[i].name }]},
                                                            {node:'element', tag:'td', child: [{node:'text', text:data.broadcast_type[i].q }]},
                                                            {node:'element', tag:'td', child: [{node:'text', text:data.broadcast_type[i].q*1400 }]},
                                                            {node:'element', tag:'td', child: [{node:'text', text:data.broadcast_type[i].q*1400+200 }]},
                                                            {node:'element', tag:'td', child: [{node:'text', text: ' ' }]},
                                                        ]
                                                    })
                                                }
                                            }
                                        }
                                    })
                                }
                            })
                            if(i.attr){
                                if(i.attr.id == 'change_point'){
                                    i.child.map(ol => {
                                        if(ol.tag == 'ol'){
                                            ol.child.map(li => {
                                                if(li.attr){
                                                    if(li.attr.id=='here_some_text') li.child[0].text = text2
                                                    else if(li.attr.id=='here_some_text'){
                                                        // li.child[0].text = text2
                                                    }
                                                    // console.log(li.child[0].text)
                                                } 
                                            })
                                        } 
                                    })            
                                }
                            }
                            if(i.attr){
                                if(i.attr.id == 'reks'){
                                    i.child.map(item => {
                                        if(item.tag == 'div'){
                                            if(item.attr.id != 'executor'){
                                                item.child.map(p => {
                                                    if(p.tag == 'p') {
                                                        p.child[0].text = ' '
                                                        p.child[0].text += 'Адрес: '+props.address+'<br>'
                                                        p.child[0].text += 'Телефон: '+props.phone+'<br>'
                                                        p.child[0].text += 'Р/c: '+props.account+'<br>'
                                                        p.child[0].text += props.bank+'<br>'
                                                        p.child[0].text += 'MФО: '+props.mfo+'<br>'
                                                        p.child[0].text += 'ИНН '+props.inn+'<br>'
                                                    }
                                                    if(p.tag == 'span') p.child[0].text =  p.child[0].text+' '+props.director
                                                })
                                            } else {
                                                item.child.map(p => {
                                                    if(p.attr){
                                                        if(p.attr.id == 'name') p.child[0].child[0].text = exes[exec-1]
                                                        if(p.attr.id == 'rekvizits') p.child[0].text = recs[exec-1]
                                                        if(p.attr.id == 'sign') p.child[0].text += dirs[exec-1]
                                                        // if(p.attr.id == 'sign') p.child[0].child[0].text = 'Юр лицо'
                                                    }
                                                })
                                            }
                                        } 
                                    })
                                } 
                            }    
                        }
                    })
                } 
                if(item.attr.id == 'app'){
                    item.child[3].child[0].text += ' '+data.n
                }
                if(item.attr.id == 'date2'){
                    item.child[0].child[0].text = data.date
                }
            }
        }) 
        let outHtml =   json2html(json)
        return outHtml
}

let  editPdf = async function(e,d,p){
    let html = await getHtml(e,d,p)
    // console.log(html)
    d.date  = d.date.replace('/', '-');
    d.date  = d.date.replace('/', '-'  );
    var promise = new Promise(function(resolve, reject) {
        pdf.create(html, options).toFile(path.join(__dirname, './contracts/'+d.n+'-'+d.date+'.pdf'), async (err, res) => {
        if (err) return reject(err);
        console.log(res);
        
        resolve(path.join(__dirname, './contracts/'+d.n+'-'+d.date+'.pdf'))
        // result =  path.join(__dirname, './contracts/'+data.n+'-'+data.date+'.pdf')
        }); 
    });
    return promise;
    // console.log(await promise)
}

module.exports = editPdf
// editPdf(3,
//     {n:12,
//         form:'ИП',
//         date:'20.05.19',sum:299000, entity:'YEAHs',
//     broadcast_type: [
//         {name: 'TYPE', q:12134},
//         {name: 'TYPE2', q:23, types: [
//         {name: 'TYPE', q:12134},
//         {name: 'TYPE', q:12134},
//         {name: 'TYPE', q:12134},
//         ]}
//     ]},
//     {
//         address: 'Yunusabad 12 sm. Ovoz',
//         address2: 'Yunusabad 12 sm. Ovoz',
//         phone:'+998909251019',
//         account: 2923993,
//         bank: ' ',
//         mfo: 2001,
//         inn: 12414235235,
//         director: 'Jayson Lee'
//     })