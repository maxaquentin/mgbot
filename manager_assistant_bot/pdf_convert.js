const PDFDocument = require('pdfkit');
const fs = require('fs');

class PdfDocument {
    constructor(url, url2) {
        try{
            let date1 = new Date()
            this.options = {
                directory: "./licenses",
                filename: date1.getTime()+".jpg"
            }
            this.url = url;
            let date2 = new Date()
            this.options2 = {
                directory: "./certificates",
                filename: date2.getTime()+".jpg"
            }
            this.url2 = url2;
            this.license_name = this.options.filename
            this.cert_name = this.options2.filename
        } catch(err) {
            throw err
        }
    }
    async downloadImages(){
        download(this.url, this.options, (err) => {
            if(err) throw err
            download(this.url2, this.options2, (err) => {
                if (err) throw err
            })
        })
        return {
            cert_file: this.cert_name,
            license_name: this.license_name
        }
    }
    async create(data, license, cert_name){
        try {
            let filename = '';
            const doc = new PDFDocument();
            await doc.pipe(fs.createWriteStream('./contracts/dogovor-'+data._id+'.pdf'));
            await doc.font('arial.ttf')
            await doc.fontSize(20)
            .text('Контракт № '+data._id,{align: 'center'})
            .moveDown(0.9);
            await doc.fontSize(14)
            .fontSize(18)
            .text('Юридическое лицо:')
            .moveDown(0.3)
            .fontSize(14)
            .text(data.entity)
            .moveDown(0.6)
            .fontSize(18)
            .text('Менеджер:')
            .moveDown(0.3)
            .fontSize(14)
            .text(data.manager)
            .moveDown(0.6)
            .fontSize(18)
            .text('Скидка:')
            .moveDown(0.3)
            .fontSize(14)
            .text(data.discount)
            .moveDown(0.6)
            .fontSize(18)
            .text('Типы размещения:')
            .moveDown(0.3)
            .fontSize(14);
            for(var i=0;i<data.broadcast_type.length;i++){
                await doc.text(data.broadcast_type[i].name+' - '+data.broadcast_type[i].q)
            }
            doc.moveDown(0.6)
            await doc.fontSize(18)
            .text('Срок заключения договора:')
            .moveDown(0.3)
            .fontSize(14)
            .text(data.exp_date)
            .moveDown(0.6)
            .fontSize(18)
            .text('Брэнды / Продукты:')
            .moveDown(0.3)
            .fontSize(14);
            for(var i=0;i<data.brands.length;i++){
                await doc.text(data.brands[i].name+' отработано: '+data.brands[i].done)
            }
            doc.moveDown(0.6)
            doc.fontSize(18).text('Рекламные компании:').fontSize(14)
            for(var i=0;i<data.companies.length;i++){
                await doc.text(data.companies[i].name+' отработано: '+data.companies[i].done)
            }
            await doc.addPage().image('./licenses/'+license, 50, 30, {width: 500})
            .text('Лицензия', 60, 10);
            await doc.addPage().image('./certificates/'+cert_name, 50, 30, {width: 500})
            .text('Сертификат', 60, 10);
            await doc.end();
            console.log('YEAP');
            return filename
        } catch (err){
            console.log('Что то не то');
            throw err;
        } 
    }
  }

module.exports = PdfDocument;