const Telegraf     = require('telegraf')
const Telegram     = require('telegraf/telegram')
const helper_bot_token = '718254468:AAEKrZyOVbcejT2YekwDxR2mr4xbyL2boR0';
let bot_options = {
  agent: null,        
  webhookReply: true 
}
const telegram         = new Telegram(helper_bot_token, bot_options)
const connection       = require('./mongo_connect');
const session          = require('telegraf/session')
const token            = '803106836:AAEii081uodVeLCgQKVdNMZnILdDL5aVq2o';
const bot              = new Telegraf('732254701:AAF-A7C6aetbJU9zO7axWz-Jz9Tucbd7hCQ')
const bot_request      = require('./bot_api')
const edit_pdf         = require('./testhtmlpdf.js')
const Markup           = require('telegraf/markup')
const TelegrafFlow     = require('telegraf-flow')
const { Scene }        = TelegrafFlow
const flow             = new TelegrafFlow()
var   download         = require('download-file')
const ObjectID         = require('mongodb').ObjectID;

let dbname = 'mg_bot_test';

bot.use(session())
bot.use(flow.middleware())
var date_pattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/m

var ad_types = [
  [{text: 'Телеграм канал', callback_data: 0}],
  [{text: 'Баннерная реклама', callback_data: 1}],
  [{text: 'Пиар статья', callback_data: 2}],
  [{text: 'Пиар статья и баннерная реклама', callback_data: 3}],
  [{text: 'Назад', callback_data: 'back'}]
];

var banner_types = [
  [{text: '640x296', callback_data: 0}],
  [{text: '950x90', callback_data: 1}],
  [{text: '240x400', callback_data: 2}],
  [{text: 'Назад', callback_data: 'back'}]
];

var pr_services = [
  [{text: 'Написание PR статьи', callback_data: 0}],
  [{text: 'Перевод с русского', callback_data: 1}],
  [{text: 'Написание PR статьи с фотоотчетом', callback_data: 2}],
  [{text: 'PR статья с интервью', callback_data: 3}],
  [{text: 'Без доп услуг', callback_data: 4}],
  [{text: 'Назад', callback_data: 'back'}]
];

var date_options = [ 
  [{text: 'До конца года', callback_data:0}],
  [{text: 'Указать дату', callback_data:1}],
  [{text: 'Назад', callback_data:'back'}],
]

var brands_options = [
  [{text: 'Да', callback_data:0}],
  [{text: 'Нет, далее', callback_data:1}],
  [{text: 'Назад', callback_data:'back'}],
]

var back = [
  {text: 'Назад', callback_data:'back'}
]

var back_forward = [
  [{text: 'Пропустить', callback_data:'forward'}],
  [{text: 'Назад', callback_data:'back'}],
]
// CREATE SCENES
const entity          = new Scene('entity');
const pdf             = new Scene('pdf');
const broadcast       = new Scene('broadcast');
const discount        = new Scene('discount');
const ad_number       = new Scene('ad_number');
const format_numbers       = new Scene('format_numbers');
const banner_options  = new Scene('banner_options');
const services        = new Scene('services');
const pr_options      = new Scene('pr_options');
const exp_date        = new Scene('exp_date');
const start_date      = new Scene('start_date');
const brands          = new Scene('brands');
const license         = new Scene('license');
const certificate     = new Scene('certificate');
const ad_companies    = new Scene('ad_companies');

function enter(ctx){
  ctx.session.state = {
    chat_id: ctx.from.id,
    executor: '',
    pay:0,
    debt:0,
    sum:0,
    deleteMessage: ctx.message.message_id,
    editMessage: ctx.message.message_id
  };
}
entity      .command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})
discount    .command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})
ad_number   .command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})
pdf         .command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})
services    .command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})
brands      .command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})
broadcast   .command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})
license     .command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})
certificate .command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})
ad_companies.command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})
start_date  .command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})
exp_date    .command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})
bot         .command('newcontract', (ctx) => {enter(ctx); ctx.flow.enter('entity')})


entity.enter(async (ctx) => {
  try{
    // let db          = await new connection
    // let dbase       = await db.db(dbname)
    // let firms       = dbase.collection('firms')
    // let all_firms   = await firms.find({executor_id:1}).toArray()
    // let entities = []
    // all_firms.map((item, index) => {
      // entities.push([{text: item.name, callback_data: index}])
    // })
    ctx.reply('Укажите юридическое лицо');   
  } catch(err){
    throw err;
  } 
})

entity.on('message', async (ctx) => {
  try {
    let db          = await new connection
    let dbase       = await db.db(dbname)
    let firms       = dbase.collection('firms')
    var re          = new RegExp(ctx.message.text,"i");
    let firm        = await firms.find({ name: { $regex: re }, executor_id:1 }).toArray()
    console.log(firm)
    let firm_buttons = [] 
    for(let i=0;i<firm.length;i++){
      firm_buttons.push([{text: firm[i].name, callback_data: i}])
    }
    ctx.session.state.firms = firm;
    firm_buttons.push([{text: 'Нет, далее', callback_data: 'no'}])
    ctx.session.state.entity = ctx.message.text;
    if(firm.length == 0) {
      let newfirm = await firms.insertOne({name: ctx.message.text, executor_id:2, props: {
        address: ' ',
        address2: ' ',
        phone:' ',
        account: ' ',
        bank: ' ',
        mfo: ' ',
        inn: ' '
    }})
      ctx.session.state.firm  = {_id: newfirm.insertedId,executor_id:2, name: ctx.message.text, props: {
        address: ' ',
        address2: ' ',
        phone:' ',
        account: ' ',
        bank: ' ',
        mfo: ' ',
        inn: ' '
      }} ;
      await ctx.reply('НОВЫЙ ДОГОВОР: \n\nЮридическое лицо: '+ ctx.session.state.entity,  Markup.removeKeyboard().extra());
      ctx.flow.enter('broadcast') 
    } else {
      let it_is = false
      firm.map(item => {
        if(item.name == ctx.message.text) {
          it_is = true
          ctx.session.state.entity = item.name;
          ctx.session.state.firm = item;
        } 
      })
      if(it_is) ctx.flow.enter('broadcast')
      else ctx.reply('Может быть, вы имели ввиду один из этих юр. лиц:', Markup.inlineKeyboard(firm_buttons).extra())
      // ctx.session.state.firm  = firm;
    }
  } catch(err){
    throw err;
  }
})

entity.on('callback_query', async(ctx) => {
  let db          = await new connection
  let dbase       = await db.db(dbname)
  let firms       = dbase.collection('firms')
  if(ctx.callbackQuery.data == 'no'){
    let newfirm = await firms.insertOne({name: ctx.session.state.entity, executor_id:2, props: {
      address: ' ',
      address2: ' ',
      phone:' ',
      account: ' ',
        director: ' ',
        bank: ' ',
      mfo: ' ',
      inn: ' '
  }})
    ctx.session.state.firm  = {_id: newfirm.insertedId,executor_id:1, name:ctx.session.state.entity , props: {
      address: ' ',
      director: ' ',
      address2: ' ',
      phone:' ',
      account: ' ',
      bank: ' ',
      mfo: ' ',
      inn: ' '
    }} ;
  } else {
    ctx.session.state.entity = ctx.session.state.firms[ctx.callbackQuery.data].name;
    ctx.session.state.firm = ctx.session.state.firms[ctx.callbackQuery.data];
  }
  console.log(ctx.session.state.entity)
  console.log(ctx.session.state.firm)
  console.log(ctx.callbackQuery.data)
    await ctx.reply('НОВЫЙ ДОГОВОР: \n\nЮридическое лицо: '+ ctx.session.state.entity,  Markup.removeKeyboard().extra());
    ctx.flow.enter('broadcast') 
})

// Broadcast SCENE
broadcast.enter((ctx) => {
  try{
    ctx.reply('Введите тип размещения', Markup.inlineKeyboard(ad_types).extra())
  } catch(err){
    throw err;
  }
})

broadcast.on('message', (ctx) => {
  try {
    ctx.flow.enter('broadcast');
  } catch (err){
    throw err;
  }
})

broadcast.on('callback_query', async (ctx) => {
  try {
    let choice  = ctx.callbackQuery.data;
    ctx.session.state.broadcast_choice  = ctx.callbackQuery.data;
    if(choice != 'back'){
      let data = []
      if(choice == 3){
        data = [{name: ad_types[1][0].text, types: []}, {name: ad_types[2][0].text, services:[]}]
      } else {
        data = [{name: ad_types[choice][0].text, type:[]}]
      }
      ctx.session.state.broadcast_type = data;
      ctx.deleteMessage()
      let mess = 'НОВЫЙ ДОГОВОР: \n\nЮридическое лицо: '
      +ctx.session.state.entity+'\nТипы размещения: \n';
      for(let i=0;i<ctx.session.state.broadcast_type.length;i++){
        mess = mess+'  '+ctx.session.state.broadcast_type[i].name
      }
      await ctx.reply(mess);
      ctx.session.state.contract = mess;
      if(choice == 1 || choice == 3) ctx.flow.enter('banner_options')
      else if(choice == 2) ctx.flow.enter('services')
      else ctx.flow.enter('ad_number');
    } else {
      ctx.deleteMessage();
      ctx.flow.enter('entity');
    }
  } catch (err){
    throw err;
  }
})

// Banner types

banner_options.enter(async (ctx) => {
  try {
  [{text: '640x296', callback_data: 0}],
  [{text: '950x90', callback_data: 1}],
  [{text: '240x400', callback_data: 2}],
  
    await ctx.reply('Выберите размеры: (укажите выборы через запятую 1 или 1,2  и т.д)\n'+
    '\n1.640x296\n2.950x90\n3.240x400',
     Markup.inlineKeyboard([{text:'Назад', callback_data: 'back'}]).extra());
  } catch(err){
    throw err;
  } 
})

banner_options.on('callback_query', (ctx) => {
  if(ctx.callbackQuery.data == 'back'){
    ctx.deleteMessage();
    ctx.flow.enter('broadcast')
  } 
})

banner_options.on('message', async (ctx) => {
  try {
    var pattern = /^[0-9]{1,3}([,][0-9]{1,3})?([,][0-9]{1,3})?$/;
    if(pattern.test(ctx.message.text)){
       options = ctx.message.text.split(',')
       ctx.session.state.broadcast_type[0].types = []
      let ok = true
       options.map(i => {
         if(parseInt(i) > 3) {
           ok= false
           ctx.reply('Нет такого варианта: '+i)
         } else {
            ctx.session.state.broadcast_type[0].types.push({
              name: banner_types[parseInt(i)-1][0].text, q: 0
            })
         }
       })
       if(ok) {
        if(ctx.session.state.broadcast_type.length == 2) ctx.flow.enter('services')
        else ctx.flow.enter('ad_number')
      }
       
    } else {
      ctx.reply('Невалидный формат ответа')
    }
  } catch (err){
    console.log(err)
  }
})

// Services

services.on('callback_query', (ctx) => {
  if(ctx.callbackQuery.data == 'back'){
    ctx.deleteMessage();
    if(ctx.session.state.broadcast_type.length == 2) ctx.flow.enter('banner_options')
    else ctx.flow.enter('broadcast')
  } 
})

services.enter(async (ctx) => {
  let text = 'Выберите услугу:\n(укажите номер, например: 1)\n\n'
  for(let t=0;t<5;t++){
    text =  text+(t+1)+'. '+pr_services[t][0].text+'\n'
  }
  await ctx.reply(text,
   Markup.inlineKeyboard([{text:'Назад', callback_data: 'back'}]).extra());
})

services.on('message',(ctx) => {
  try {
    var pattern = /^\d+$/;
    if(pattern.test(ctx.message.text)){
         if(parseInt(ctx.message.text) > 5){
           ctx.reply('Нет такого варианта: '+ctx.message.text)
         } else {
          let  length = ctx.session.state.broadcast_type.length;
          console.log(parseInt(ctx.message.text))
          ctx.session.state.broadcast_type[length - 1].type =  pr_services[parseInt(ctx.message.text)-1][0].text
          ctx.flow.enter('ad_number')
        }
    } else {
      ctx.reply('Укажите номер выбора')
    }
    console.log(pattern.test(ctx.message.text))
  } catch (err){
    console.log(err)
  }
})
// Format NUMS
format_numbers.enter(ctx => {
  console.log(ctx.session.state.broadcast_type[0])
  console.log(ctx.session.state.last_format)
  console.log('Длина '+ctx.session.state.broadcast_type[0].types.length)
  if(ctx.session.state.last_format ==  ctx.session.state.broadcast_type[0].types.length){
    ctx.flow.enter('ad_number')
  } else {
    ctx.reply('Для размера '+ ctx.session.state.broadcast_type[0].types[ctx.session.state.last_format].name, Markup.inlineKeyboard(back).extra())
  }
})

format_numbers.on('message',async (ctx) => {
  if(ctx.session.state.last_format ==  ctx.session.state.broadcast_type[0].types.length) ctx.flow.enter('ad_number')
  else {
    var pattern = /^\d+$/;
    if(pattern.test(ctx.message.text)){
      ctx.session.state.broadcast_type[0].types[ctx.session.state.last_format].q = parseInt(ctx.message.text) 
      ctx.session.state.last_format = ctx.session.state.last_format+1
      ctx.flow.enter('format_numbers')
    } else ctx.reply('Укажите в числах')
  }
})

format_numbers.on('callback_query', (ctx) => {
  if(ctx.callbackQuery.data == 'back'){
    ctx.deleteMessage();
    if(ctx.session.state.broadcast_type.length == 2) ctx.flow.enter('services')
    else ctx.flow.enter('banner_options')
  } 
})
// Ads number
ad_number.enter(async (ctx) => {
  try{
    if(ctx.session.state.broadcast_type.length != 1){
      if(!ctx.session.state.last_format){
          await  ctx.reply('Введите количество размещений для каждого типа и размера:')
          ctx.reply('1. '+ ctx.session.state.broadcast_type[0].name)
          ctx.session.state.last_format = 0;
          ctx.flow.enter('format_numbers') 
      } else {
        ctx.reply('2. '+ctx.session.state.broadcast_type[1].name+ ' \n Введите количество размещений', Markup.inlineKeyboard(back).extra());
      } 
    } else {
      if(ctx.session.state.broadcast_type[0].name == ad_types[1][0].text){
        if(!ctx.session.state.last_format){
          ctx.session.state.last_format = 0
          ctx.flow.enter('format_numbers') 
        } else {
          ctx.flow.enter('discount') 
        }
      } else {
        ctx.reply('Введите количество размещений:', Markup.inlineKeyboard(back).extra())
      }
    }
  } catch(err){
    throw err;
  } 
})

ad_number.on('callback_query', (ctx) => {
  if(ctx.callbackQuery.data == 'back'){
    delete ctx.session.state.ad_number;
    if(ctx.session.state.last_format) delete ctx.session.state.last_format
    ctx.deleteMessage();
    ctx.flow.enter('broadcast');
  }
});

ad_number.on('message', async (ctx) => {
  try{
    let data = ctx.message.text;
    var pattern = /^\d+$/;
    // if(ctx.session.state.broadcast_type.length != 1){
    //   if(!ctx.session.state.ad_number){
    //     if(pattern.test(data) == true){
    //     } else {
    //       ctx.reply('Укажите в числах');
    //     }
    //   } else {
    //     if(pattern.test(data) == true){
    //       ctx.session.state.broadcast_type[1].q = data;
    //       delete ctx.session.state.ad_number;
    //       ctx.deleteMessage();
    //       ctx.flow.enter('discount');
    //     } else {
    //       ctx.reply('Укажите в числах');
    //     }
    //   }
    // } else {
      if(pattern.test(data) == true){
        ctx.session.state.broadcast_type[ctx.session.state.broadcast_type.length-1].q = data;
        ctx.flow.enter('discount');
      } else {
        ctx.reply('Укажите в числах');
      }
    // }
  } catch (err){
    throw err;
  }
})

// Discount SCENE

discount.enter(async (ctx) => {
  var mess = 'НОВЫЙ ДОГОВОР: \n\nЮридическое лицо: '
  +ctx.session.state.entity+'\nТипы размещения: \n';
  for(let i=0;i<ctx.session.state.broadcast_type.length;i++){
    mess = mess+'  '+ctx.session.state.broadcast_type[i].name+':\n'
    if(ctx.session.state.broadcast_type[i].name == ad_types[1][0].text){ 
      for(let j=0;j<ctx.session.state.broadcast_type[0].types.length;j++) {
        mess= mess+'   '+ctx.session.state.broadcast_type[0].types[j].name+' - '+ctx.session.state.broadcast_type[0].types[j].q+'\n'
      }
    } else {
      if(ctx.session.state.broadcast_type[i].name == ad_types[2][0].text) mess = mess+'  '+ctx.session.state.broadcast_type[i].type
      mess = mess+ '   '+ctx.session.state.broadcast_type[i].q
    }
  }
  await ctx.reply(mess);
  ctx.session.state.contract = mess;
  ctx.reply('Скидка: ',  Markup.inlineKeyboard(back).extra());
})

discount.on('callback_query', (ctx) => {
  if(ctx.callbackQuery.data == 'back'){
    if(ctx.session.state.last_format){
      delete ctx.session.state.last_format
    } 
    ctx.deleteMessage();
    ctx.flow.enter('ad_number');
  }
});

discount.on('message', async (ctx) => {
  try{
    var pattern = /^\d+$/;
    let data  = ctx.message.text;
    if(pattern.test(data) == true){
      ctx.session.state.discount = data;
      ctx.flow.enter('start_date');
    } else {
      ctx.reply('Укажите числами, пожалуйста');
    }
  } catch(err){
    throw err;
  } 
})

// START DATE
start_date.enter(async(ctx) => {
  var mess = 'НОВЫЙ ДОГОВОР: \n\nЮридическое лицо: '
  +ctx.session.state.entity+'\nТипы размещения: \n';
  for(let i=0;i<ctx.session.state.broadcast_type.length;i++){
    mess = mess+'  '+ctx.session.state.broadcast_type[i].name+':\n'
    if(ctx.session.state.broadcast_type[i].name == ad_types[1][0].text){ 
      for(let j=0;j<ctx.session.state.broadcast_type[0].types.length;j++) {
        mess= mess+'   '+ctx.session.state.broadcast_type[0].types[j].name+' - '+ctx.session.state.broadcast_type[0].types[j].q+'\n'
      }
    } else {
      if(ctx.session.state.broadcast_type[i].name == ad_types[2][0].text) mess = mess+'  '+ctx.session.state.broadcast_type[i].type
      mess = mess+ '   '+ctx.session.state.broadcast_type[i].q
    }
  }
  mess = mess+'\nCкидка: '+ctx.session.state.discount
  await ctx.reply(mess);
  ctx.reply('Введите дату заключения договора в формате дд.мм.гггг', Markup.inlineKeyboard(back).extra())
})

start_date.on('callback_query', (ctx) => {
  if(ctx.callbackQuery.data == 'back'){
    ctx.deleteMessage();
    ctx.flow.enter('discount');
  }
});

start_date.on('message', async (ctx) => {
    let check = date_pattern.test(ctx.message.text)
    if(check == true){
      let db          = await new connection
      let dbase       = await db.db(dbname)
      let contracts   = dbase.collection('contracts');
      let latest      = await contracts.find({executor_id:2})
                    .limit(1).sort({$natural:-1}).toArray()
      let message  = ctx.message.text.replace('.', '-');
       message     = message.replace('.', '-');
       message     = message.split('-');
       
       message[1]  = parseInt(message[1]);
      let time     = (new Date(message[2]+'-'+message[1]+'-'+message[0])).getTime();
      // let docId = ctx.session.state.lastDocument;
      if(latest[0]){
        if(latest[0].start_date){
        // let latest[0].start_date = (new Date(latest[0].start_date)).getTime();
        if(latest[0].start_date <= time){
          ctx.session.state.start_date = time;
          ctx.deleteMessage();
          ctx.flow.enter('exp_date');
        } else {  
          ctx.reply('Некорректная дата заключения, нельзя заключать раньше предыдущего договора('+new Date(latest[0].start_date).toLocaleDateString('ru-RU')+')\nПожалуйста введите правильную дату');
        }
      } else {
        if((new Date().getTime() - time) <= 2*86400000){
          ctx.session.state.start_date = time;
          ctx.deleteMessage();
          ctx.flow.enter('exp_date');
        } else {
          ctx.reply('Некорректная дата заключения, введите правильную дату');
        }
      } 
    } else {
          ctx.session.state.start_date = time;
          ctx.deleteMessage();
          ctx.flow.enter('exp_date');
      }
    } else {
      ctx.reply('Введите дату в формате дд.мм.гггг');
    }
});

// EXPIRATION SCENE str.replace(/a/g, 'x');

exp_date.enter(async(ctx) => {
  var mess = 'НОВЫЙ ДОГОВОР: \n\nЮридическое лицо: '
  +ctx.session.state.entity+'\nТипы размещения: \n';
  for(let i=0;i<ctx.session.state.broadcast_type.length;i++){
    mess = mess+'  '+ctx.session.state.broadcast_type[i].name+':\n'
    if(ctx.session.state.broadcast_type[i].name == ad_types[1][0].text){ 
      for(let j=0;j<ctx.session.state.broadcast_type[0].types.length;j++) {
        mess= mess+'   '+ctx.session.state.broadcast_type[0].types[j].name+' - '+ctx.session.state.broadcast_type[0].types[j].q+'\n'
      }
    } else {
      if(ctx.session.state.broadcast_type[i].name == ad_types[2][0].text) mess = mess+'  '+ctx.session.state.broadcast_type[i].type
      mess = mess+ '   '+ctx.session.state.broadcast_type[i].q
    }
  }
  mess = mess+'\nCкидка: '+ctx.session.state.discount+
  '\nДата заключения: '+new Date(ctx.session.state.start_date).toLocaleDateString('ru-RU').toString();
  await ctx.reply(mess);
  ctx.reply('Введите дату завершения договора: ', Markup.inlineKeyboard(date_options).extra())
})

exp_date.on('message', async (ctx) => {
  if(!ctx.session.state.date_option){
    ctx.reply('Выберите один из вариантов', Markup.inlineKeyboard(date_options).extra());
  } else {
    let msg = ctx.message.text.toString();
    let check = await date_pattern.test(msg)
    if(await check == true){
      let message  = ctx.message.text.replace('.', '-');
       message  = message.replace('.', '-');
       message  = message.split('-');
       message[1] = parseInt(message[1]);
       let time = (new Date(message[2]+'-'+message[1]+'-'+message[0])).getTime();
      if((time - ctx.session.state.start_date) >= 86400000){
        ctx.session.state.exp_date = time
        // ctx.reply(new Date(ctx.session.state.exp_date).toLocaleDateString().toString());
        ctx.flow.enter('license')
        } else {
          ctx.reply('Минимальный срок договора - 1 день \nВведите правильную дату истечения срока договора:');
        }
    } else {
      ctx.reply('Пожалуйста, введите дату в формате дд.мм.гггг');
    }
  }
});

exp_date.on('callback_query', async (ctx) => {
  let choice = ctx.callbackQuery.data;
  ctx.session.state.date_option = ctx.callbackQuery.data;
  if(choice == 0){
    let time = new Date(new Date().getFullYear(), 11, 31).getTime()
    if((time - ctx.session.state.start_date) >= 86400000){
      ctx.session.state.exp_date = time;
      ctx.flow.enter('license');
      } else {
        ctx.reply('Минимальный срок договора - 1 день \nВведите правильную дату истечения срока договора:');
    }
  }else if(choice == 'back'){
      ctx.flow.enter('start_date');
  } else {
    ctx.reply('Введите дату в формате дд.мм.гггг');
  }
});

// BRANDS SCENE

brands.enter(async (ctx) => {
  var mess = 'НОВЫЙ ДОГОВОР: \n\nЮридическое лицо: '
  +ctx.session.state.entity+'\nТипы размещения: \n';
  for(let i=0;i<ctx.session.state.broadcast_type.length;i++){
    mess = mess+'  '+ctx.session.state.broadcast_type[i].name+':\n'
    if(ctx.session.state.broadcast_type[i].name == ad_types[1][0].text){ 
      for(let j=0;j<ctx.session.state.broadcast_type[0].types.length;j++) {
        mess= mess+'   '+ctx.session.state.broadcast_type[0].types[j].name+' - '+ctx.session.state.broadcast_type[0].types[j].q+'\n'
      }
    } else {
      if(ctx.session.state.broadcast_type[i].name == ad_types[2][0].text) mess = mess+'  '+ctx.session.state.broadcast_type[i].type
      mess = mess+ '   '+ctx.session.state.broadcast_type[i].q
    }
  }
  mess = mess+'\nCкидка: '+ctx.session.state.discount+
  '\nДата заключения: '+new Date(ctx.session.state.start_date).toLocaleDateString('ru-RU').toString()
  +'\nДата завершения: '+new Date(ctx.session.state.exp_date).toLocaleDateString('ru-RU').toString()
  await ctx.reply(mess);
  ctx.reply('Введите бренд/продукт рекламы:',  Markup.inlineKeyboard(back).extra())
})

brands.on('message', async (ctx) => {
  
  if(!ctx.session.state.brands) ctx.session.state.brands = [];
  if(!ctx.session.brand_typed){
    ctx.session.brand_typed = 1;
    let num = 0;
    for(var i=0;i<ctx.session.state.broadcast_type.length;i++){
      num = num + ctx.session.state.broadcast_type[i].q
    }
    ctx.session.state.brands.push({
      name: ctx.message.text,
      ad_numbers: num,
      left: num,
      done: 0,
      companies: []
    })
    ctx.session.brand_typed = 1;
    ctx.reply('Добавить еще бренд/продукт? ', Markup.inlineKeyboard(brands_options).extra())
  } else if(ctx.session.brand_typed == 0){
    ctx.session.brand_typed = 1;
    let num = 0;
    ctx.session.state.broadcast_type.map(async (item) => {
      num = parseInt(num) + await Number(item.q)
    })
    ctx.session.state.brands.push({
      name: ctx.message.text,
      ad_numbers: num,
      left: num,
      done: 0,
      companies: []
    })
    ctx.session.brand_typed = 1
    ctx.reply('Добавить еще бренд/продукт? ', Markup.inlineKeyboard(brands_options).extra())
  } else {
    ctx.reply('Добавить еще бренд/продукт? ', Markup.inlineKeyboard(brands_options).extra())
  }
});

brands.on('callback_query', async (ctx) => {
  if(!ctx.session.state.brands) {ctx.flow.enter('brands');}
  else if(ctx.callbackQuery.data == 0){
    ctx.reply('Введите бренд/продукт');
    ctx.deleteMessage();
    ctx.session.brand_typed = 0
  } else if(ctx.callbackQuery.data == 'back'){
    ctx.deleteMessage()
    ctx.flow.enter('certificate');
  }else {
      ctx.deleteMessage()
      ctx.flow.enter('ad_companies');
  }
});

// COMPANIES SCENE

ad_companies.enter(async (ctx) => {
  var mess = 'НОВЫЙ ДОГОВОР: \n\nЮридическое лицо: '
  +ctx.session.state.entity+'\nТипы размещения: \n';
  for(let i=0;i<ctx.session.state.broadcast_type.length;i++){
    mess = mess+'  '+ctx.session.state.broadcast_type[i].name+':\n'
    if(ctx.session.state.broadcast_type[i].name == ad_types[1][0].text){ 
      for(let j=0;j<ctx.session.state.broadcast_type[0].types.length;j++) {
        mess= mess+'   '+ctx.session.state.broadcast_type[0].types[j].name+' - '+ctx.session.state.broadcast_type[0].types[j].q+'\n'
      }
    } else {
      if(ctx.session.state.broadcast_type[i].name == ad_types[2][0].text) mess = mess+'  '+ctx.session.state.broadcast_type[i].type
      mess = mess+ '   '+ctx.session.state.broadcast_type[i].q
    }
  }
  
  mess = mess+'\nCкидка: '+ctx.session.state.discount+
  '\nДата заключения: '+new Date(ctx.session.state.start_date).toLocaleDateString('ru-RU').toString()
  +'\nДата завершения: '+new Date(ctx.session.state.exp_date).toLocaleDateString('ru-RU').toString()+'\nБрэнды/Продукты:\n'
  for(let i=0;i<ctx.session.state.brands.length;i++){
    mess = mess+'  '+ctx.session.state.brands[i].name+'\n'
    for(let j=0;j<ctx.session.state.brands[i].companies.length;j++){
      mess = mess+' '+ctx.session.state.brands[i].companies[j].name+'\n'
    }
  }
  await ctx.reply(mess);
  let buttons = [];
  for(let i=0;i<ctx.session.state.brands.length;i++){
    buttons.push([{text:ctx.session.state.brands[i].name, callback_data: i+5}])
  }
  buttons.push([{text:'Назад', callback_data: 'back'}])
  ctx.reply('Ввод компаний - выберите бренд/продукт:', Markup.inlineKeyboard(buttons).extra())
})

ad_companies.on('message', async (ctx) => {
  if(ctx.session.state.chosen_brand){
  if(!ctx.session.state.companies) ctx.session.state.companies = [];
  if(!ctx.session.company_typed){
    ctx.session.company_typed = 1;
    let num = 0;
    for(var i=0;i<ctx.session.state.broadcast_type.length;i++){
      num = num + ctx.session.state.broadcast_type[i].q
    }
    ctx.session.state.brands[ctx.session.state.chosen_brand-5].companies.push({
      name: ctx.message.text,
      ad_numbers: num,
      left: num,
      done: 0,
    })
    ctx.session.company_typed = 1;
    ctx.reply('Добавить компанию еще? ', Markup.inlineKeyboard(brands_options).extra())
  } else if(ctx.session.company_typed == 0){
    ctx.session.company_typed = 1;
    let num = 0;
    ctx.session.state.broadcast_type.map(async (item) => {
      num = parseInt(num) +  parseInt(item.q)
    })
    ctx.session.state.brands[ctx.session.state.chosen_brand-5].companies.push({
      name: ctx.message.text,
      ad_numbers: num,
      left: num,
      done: 0,
    })
    ctx.session.brand_typed = 1
    ctx.reply('Добавить компанию еще? ', Markup.inlineKeyboard(brands_options).extra())
  } else {
    ctx.reply('Добавить компанию еще? ', Markup.inlineKeyboard(brands_options).extra())
  }
}
});

ad_companies.on('callback_query', async (ctx) => {
  if(ctx.callbackQuery.data == 0){
    ctx.reply('Введите рекламную компанию для '+ctx.session.state.brands[ctx.session.state.chosen_brand-5].name);
    ctx.session.brand_typed = 0
  } else if(ctx.callbackQuery.data == 'back'){
    ctx.session.brand_typed = 0
    if(ctx.session.state.chosen_brand){
      ctx.deleteMessage()
      delete ctx.session.state.chosen_brand
      ctx.flow.enter('ad_companies')
    } else {
      ctx.deleteMessage()
      ctx.flow.enter('brands')
    }
  } else if(ctx.callbackQuery.data > 4){
    ctx.reply('Введите компанию для '+ctx.session.state.brands[ctx.callbackQuery.data-5].name);
    ctx.session.state.chosen_brand = ctx.callbackQuery.data;
  } else if(ctx.callbackQuery.data == 'go'){
      ctx.deleteMessage()
      ctx.flow.enter('pdf');
  }
  else {
    var mess = 'НОВЫЙ ДОГОВОР: \n\nЮридическое лицо: '
    +ctx.session.state.entity+'\nТипы размещения: \n';
    for(let i=0;i<ctx.session.state.broadcast_type.length;i++){
      mess = mess+'  '+ctx.session.state.broadcast_type[i].name+':\n'
      if(ctx.session.state.broadcast_type[i].name == ad_types[1][0].text){ 
        for(let j=0;j<ctx.session.state.broadcast_type[0].types.length;j++) {
          mess= mess+'   '+ctx.session.state.broadcast_type[0].types[j].name+' - '+ctx.session.state.broadcast_type[0].types[j].q+'\n'
        }
      } else {
        if(ctx.session.state.broadcast_type[i].name == ad_types[2][0].text) mess = mess+'  '+ctx.session.state.broadcast_type[i].type
        mess = mess+ '   '+ctx.session.state.broadcast_type[i].q
      }
    }
    mess = mess+'\nCкидка: '+ctx.session.state.discount+
    '\nДата заключения: '+new Date(ctx.session.state.start_date).toLocaleDateString('ru-RU').toString()
    +'\nДата завершения: '+new Date(ctx.session.state.exp_date).toLocaleDateString('ru-RU').toString()
    +'\nБрэнды/Продукты:\n'
    for(let i=0;i<ctx.session.state.brands.length;i++){
      let comps = '   '
      for(let j=0;j<ctx.session.state.brands[i].companies.length;j++){
        comps = await comps+ctx.session.state.brands[i].companies[j].name+'\n   '
      }
      mess = await mess+'  '+ctx.session.state.brands[i].name+'\n'+comps
    }
    await ctx.reply(mess, Markup.inlineKeyboard([[{text: 'Получить документ',callback_data:'go'}],
     [{text: 'Назад',callback_data:'back'}]]).extra());
  }
});

// LICENSE SCAN SCENE

license.enter(async (ctx) => {
  var mess = 'НОВЫЙ ДОГОВОР: \n\nЮридическое лицо: '
  +ctx.session.state.entity+'\nТипы размещения: \n';
  for(let i=0;i<ctx.session.state.broadcast_type.length;i++){
    mess = mess+'  '+ctx.session.state.broadcast_type[i].name+':\n'
    if(ctx.session.state.broadcast_type[i].name == ad_types[1][0].text){ 
      for(let j=0;j<ctx.session.state.broadcast_type[0].types.length;j++) {
        mess= mess+'   '+ctx.session.state.broadcast_type[0].types[j].name+' - '+ctx.session.state.broadcast_type[0].types[j].q+'\n'
      }
    } else {
      if(ctx.session.state.broadcast_type[i].name == ad_types[2][0].text) mess = mess+'  '+ctx.session.state.broadcast_type[i].type
      mess = mess+ '   '+ctx.session.state.broadcast_type[i].q
    }
  }
  mess = mess+'\nCкидка: '+ctx.session.state.discount+
  '\nДата заключения: '+new Date(ctx.session.state.start_date).toLocaleDateString('ru-RU').toString()
  +'\nДата завершения: '+new Date(ctx.session.state.exp_date).toLocaleDateString('ru-RU').toString()
  await ctx.reply(mess);
    ctx.reply('Отправьте скан лицензии. Формат: jpg,png (фото)',  Markup.inlineKeyboard(back_forward).extra());
})

license.on('photo', async (ctx) => {
  try{
    ctx.session.state.photos = [];
    let request   = await new bot_request();
    var pid = 3
    if(!ctx.message.photo[3]){
      pid = 1;
    }
    let file      = await request.getFile(ctx.message.photo[pid].file_id);
    let file_path ='https://api.telegram.org/file/bot'+token+'/'+file.result.file_path;
    
    let date1 = new Date()
            this.options = {
                directory: "./licenses",
                filename: date1.getTime()+".jpg"
            }
    await download(file_path, this.options);
    let data = {
      path: file_path,
      file_id: ctx.message.photo[pid].file_id,
      filename: this.options.filename 
    }
    ctx.session.state.license = data;
    ctx.flow.enter('certificate');
  } catch(err){
    throw err;
  }
})

license.on('message', async (ctx) => {
  ctx.reply('Отправьте скан лицензии, пожалуйста. Формат: jpg,png (фото)');
})

license.on('callback_query', async (ctx) => {
  if(ctx.callbackQuery.data == 'back'){
    ctx.deleteMessage();
    ctx.flow.enter('exp_date');
  }else if(ctx.callbackQuery.data == 'forward'){
    ctx.deleteMessage();
    ctx.session.state.license = null;
    ctx.flow.enter('certificate');
  }else {
    ctx.reply('Отправьте скан лицензии, пожалуйста. Формат: jpg,png (фото)');
  }
})

// Certificate scan SCENE

certificate.enter((ctx) => {
  ctx.reply('Отправьте скан гувохнома. Формат: jpg,png (фото)', Markup.inlineKeyboard(back).extra());
})

certificate.on('photo', async (ctx) => {
  try {
    let request = await new bot_request();
    var pid = 3;
    if(!ctx.message.photo[3]){
      pid = 1;
    }
    let file    = await request.getFile(ctx.message.photo[pid].file_id);
    let file_path ='https://api.telegram.org/file/bot'+token+'/'+file.result.file_path;
    let date1 = new Date()
            this.options = {
                directory: "./certificates",
                filename: date1.getTime()+".jpg"
            }
    await download(file_path, this.options);
    let data = {
      path: file_path,
      file_id: ctx.message.photo[pid].file_id,
      filename: this.options.filename 
    }
    ctx.session.state.certificate = data;
        ctx.flow.enter('brands');
  } catch(err) {
    throw err;
  }
})

certificate.on('message', async (ctx) => {
  ctx.reply('Отправьте скан гувохнома, пожалуйста. Формат: jpg,png (фото)');
})

certificate.on('callback_query', async (ctx) => {
  if(ctx.callbackQuery.data == 'back'){
    ctx.deleteMessage()
    ctx.flow.enter('license');
  }else {
    ctx.reply('Отправьте скан гувохнома, пожалуйста. Формат: jpg,png (фото)');
  }
})

pdf.enter(async (ctx) => {
  try{
     let db         = await new connection
    let dbase       = await db.db(dbname)
    let contracts   = dbase.collection('contracts');
    let users       = dbase.collection('users');
    let date        = new Date();
    let created_at  = date.getTime()
    let latest      = await contracts.find({executor_id:2}).limit(1).sort({$natural:-1}).toArray()
    let ad_q = 0
    console.log(ctx.session.state)
    for(let i=0;i<ctx.session.state.broadcast_type.length;i++){
      if(ctx.session.state.broadcast_type[i].types) {
        for(let j=0;j<ctx.session.state.broadcast_type[i].types.length;j++){
          ad_q = ad_q + parseInt(ctx.session.state.broadcast_type[i].types[j].q)
        }
      } else {
        ad_q = ad_q + parseInt(ctx.session.state.broadcast_type[i].q)
      }
    }
    var n = 0
    if(latest[0]){
      var n = latest[0].n
    }
    let data = {
      date: new Date(ctx.session.state.start_date).toLocaleDateString('ru-RU'),
      n: n+1,
      broadcast_type: ctx.session.state.broadcast_type,
      entity: ctx.session.state.entity,
      certificate: ctx.session.state.certificate.filename,
    }
    
    let newPdf = await edit_pdf(2, data, ctx.session.state.firm.props)
 
    let contract    = await contracts.insertOne({
      ...ctx.session.state,
      n:n+1,
      ad_q:ad_q,
      ad_left:ad_q,
      executor_id: 2,
      created_at: created_at,
      document_path: await newPdf,
      manager: ctx.from.first_name+' '+ctx.from.last_name,
    });
    let docId         = contract.insertedId;
    let res           = await contracts.findOne({_id: ObjectID(docId)});
    // let pdf_doc    = await new pdf_convert();
    // let contract_document   = await pdf_doc.create(await res, await res.license.filename, await res.certificate.filename);
    ctx.reply('Договор успешно создан', Markup.inlineKeyboard([{text: 'Создать новый контракт', callback_data:'create'}]));
    ctx.replyWithDocument({ source: await newPdf })
    let helpers      = await users.find({type: 'assistant'}).toArray();
    for(var i=0;i<helpers.length;i++){
      telegram.sendMessage(helpers[i].chat_id, 'Создан ноый договор.')
      telegram.sendDocument(helpers[i].chat_id, { source: await newPdf });
    }
  } catch(err){
    throw err
  }
})

//      Scene registration
flow.register(discount);
flow.register(pdf);
flow.register(brands);
flow.register(format_numbers);
flow.register(ad_companies);
flow.register(services);
flow.register(ad_number);
flow.register(banner_options);
flow.register(exp_date);
flow.register(start_date);
flow.register(broadcast);
flow.register(entity);
flow.register(license);
flow.register(certificate);
//      Start off

bot.start(async (ctx) => {
  try{
    let chat_id     = ctx.from.id;
    let db          = await new connection
    let dbase       = await db.db(dbname);
    let users       = dbase.collection('users');
    let contracts   = dbase.collection('contracts');
    let latest      = await contracts.find({executor_id:2}).limit(1).sort({$natural:-1}).toArray()
   
    let user  = await users.updateOne({chat_id: chat_id},
      {$set: {
        chat_id: chat_id,
        username: ctx.from.username,
        name: ctx.from.first_name+' '+ctx.from.last_name, 
        date: new Date()}},
      {upsert: true});
      ctx.session.state = {
        chat_id: chat_id,
        executor: '',
        pay:0,
        debt:0,
        sum:0,
        deleteMessage: ctx.message.message_id,
        editMessage: ctx.message.message_id
      };
      ctx.reply('Здравствуйте!', Markup.inlineKeyboard([{text: 'Создать новый контракт', callback_data:'create'}]).extra());
  } catch(err){
    throw err;
  }
  });

bot.on('callback_query', async (ctx) => {
    if(ctx.callbackQuery.data == 'create'){
      ctx.deleteMessage();
    ctx.flow.enter('entity');
  }
})


bot.startPolling();