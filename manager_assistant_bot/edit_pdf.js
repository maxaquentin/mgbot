const HummusRecipe = require('hummus-recipe');
const numtoword    = require('./num_to_words');
const path         = require('path');

const exes         = ['YaTT “Muslimova Lola Alimbayevna”',
`ООО «Simple Networking Solutions»`,
'YaTT “Agzamxodjaev Muxrimxon Masudovich”'
]

const   recs  = [   `YaTT “Muslimova Lola Alimbayevna”\n
Адрес: г. Ташкент, Мирзо Улугбекский район,\n
ул. Паркент, д.24, кв.67\n
Тел.: +99893 533-74-33\n
Р/с: 2021 8000 0008 3931 9001\n
Мирзо Улугбекский ф-л АКБ «Капиталбанк»\n
МФО: 01018  \n
ИНН:482 910 279\n   
`,
`ООО «Simple Networking Solutions»\n
Адрес: г. Ташкент, Мирзо Улугбекский район,\n 
проспект Мустакиллик, дом 59А\n
Тел/факс: 140-86-85\n
Р/с: 2020 8000 2049 0302 8001\n
Чиланзарский ф-л ЧАК «Давр-банк»\n
МФО: 01046  ОКЭД: 58130\n
ИНН: 301 878 322   \n
`,
`YaTT “Agzamxodjaev Muxrimxon Masudovich”\n
Адрес: г. Ташкент, Чиланзарский район, \n
5-й проезд  Лутфий,  д.7\n
Тел.: +99890 930-60-62\n
Р/с: 2021 8000 9008 3919 7001\n
Чиланзарский ф-л ЧАКБ «Давр-банк»\n
МФО: 01046  \n
ИНН: 503 405 239\n   
`
]

const editPdf = function (exec, data, props){
    let ad_length = 0;
    for(let i=0;i<data.broadcast_type.length;i++){
        if(data.broadcast_type[i].types){
            ad_length = ad_length+data.broadcast_type[i].types.length    
        }else {
            ad_length = ad_length +1
        }   
    }
    let height_diff = (4-ad_length)*41
    data.date  = data.date.replace('/', '-');
    data.date  = data.date.replace('/', '-'  );
    if(!data.sum) data.sum = 289227
    if(!data.n) data.n = 2;
    else if(data.n == null) data.n = 2 
    let input_filename = './TEMP'+ad_length.toString()+'.pdf'
    console.log(input_filename)
    console.log(data)
    const pdfDoc = new HummusRecipe(input_filename, path.join(__dirname, './contracts/'+
    data.n+'-'+data.date+'.pdf'));
    let text_initial =  exes[exec-1]+`, именуемое далее «Исполнитель», в лице директора Калимулина Б. В.,\n
     действующее на основании Устава, с одной стороны, и `+data.entity+`,\n 
 
     далее именуемое «Заказчик», в лице _____________________, действующее на основании Устава,\n 
     с другой стороны, в дальнейшем именуемые Стороны, заключили настоящий договор о нижеследующем:\n`
    let text2 = `3.1.         Общая стоимость Услуг по Договору составляет `+numtoword(data.sum)+`\n
        сум, 00 тийин, в том числе НДС 7% `+numtoword(data.sum)+` \n
        сум, 00 тийин, которая установлена Сторонами с их согласия. Данный пункт Договора заменяет Протокол\n
        согласования договорной цены. Стоимость оказываемых Услуг по Договору считается фиксированной и не\n
        подлежит изменению в течение всего срока действия Договора \n`
    let opts = {
        color: '000000',
        fontSize: 10,
        font: 'Arial',
    }
    console.log
    console.log(exec)
    pdfDoc
        .editPage(1)
        .text(data.n, 346, 87, {
            bold: true,
            ...opts,
            fontSize:14,
        })
        .text(data.date, 466, 117, {
            bold: true,
            ...opts,
        })
        .text(text_initial, 35, 143, {
            ...opts,
        });
        let height = 450
        let number = 1
        for(let i=0;i<data.broadcast_type.length;i++){
            if(data.broadcast_type[i].types){
                for(let j=0;j<data.broadcast_type[i].types.length;j++){
                    pdfDoc
                    .text(number,38,height,opts)
                    .text(data.broadcast_type[i].name,100,height,opts)
                    .text(data.broadcast_type[i].types[j].name,100,height+11,opts)
                    .text(data.broadcast_type[i].types[j].q,225,height,opts)
                    .text(data.broadcast_type[i].types[j].q*1400,300, height,opts)
                    .text((data.broadcast_type[i].types[j].q*1400)+(200),389, height,opts)
                if(number != 1) pdfDoc.text('Daryo',58,height,opts)
                let differ = 33
                if(ad_length == 4) differ = 28 
                height = height+differ
                number = number+1
                }
            } else {
                pdfDoc
                .text(number,38,height,opts)
                .text(data.broadcast_type[i].name,100, height, opts);
                if(data.broadcast_type[i].type){
                  if(data.broadcast_type[i].type == 'Написание PR статьи с фотоотчетом') data.broadcast_type[i].type = 'c фотоотчетом'  
                    pdfDoc.text(data.broadcast_type[i].type,100, height+9, {
                        ...opts,
                        fontSize:10
                    })
                } 
                pdfDoc.text(data.broadcast_type[i].q,225, height, opts)
                .text(data.broadcast_type[i].q*1400,300, height,opts)
                .text((data.broadcast_type[i].q*1400)+(200),389, height,opts)
                if(number != 1) pdfDoc.text('Daryo',58,height,opts)
                let differ = 33
                if(ad_length == 4) differ = 28 
                height = height+differ
                number = number+1
            }
        }
        let diff = 0
        if(ad+_length == 1) diff = diff+10
        else if(ad_length == 3) diff = -10
        else if(ad_length == 4) diff = -35
        pdfDoc.text(text2,39,646-height_diff+diff,{
            ...opts
        })
        let ent_diff = 45;
        let rec_diff = 30;
        if(ad_length == 4) ent_diff = 0
        if(ad_length == 4) rec_diff = 0
        let propline = 18
        pdfDoc
        .endPage()
        // edit 2nd page
        .editPage(4)
        .text(data.entity,315,433-height_diff+ent_diff,{
            bold: true,
            ...opts
        })
        if(props){
            pdfDoc
            .text('Адрес: '+props.address,315,433-height_diff+ent_diff+propline+11,{
                ...opts
            })
            .text(props.address2,315,433-height_diff+ent_diff+propline*2+11,{
                ...opts
            })  
            .text('Тел.: '+props.phone,315,433-height_diff+ent_diff+propline*3+11,{
                ...opts
            })
            .text('Р/c: '+props.account,315,433-height_diff+ent_diff+propline*4+11,{
                ...opts
            })  
            .text(props.bank,315,433-height_diff+ent_diff+propline*5+11,{
                ...opts
            })  
            .text('МФО: '+props.mfo,315,433-height_diff+ent_diff+propline*6+11,{
                ...opts
            })  
            .text('ИНН: '+props.inn,315,433-height_diff+ent_diff+propline*7+11,{
                ...opts
            })  
        }
        pdfDoc
        .text(recs[exec-1],35,463-height_diff+rec_diff,{
            ...opts,
            lineHeight: 32
        })
        .endPage()
        .editPage(5)
        .text(data.n, 346, 95, {
            bold: true,
            ...opts,
            fontSize:14,
        })
        .text(data.date, 466, 127, {
            bold: true,
            ...opts,
        })
        .endPage()
        // .createPage('A4', 90)
        // .image('/certificates/'+data.certificate, 20, 100, {width: 300, keepAspectRatio: true})
        // .endPage();
        // if(data.license){
        //     pdfDoc.createPage('A4', 90)
        //     .image('/licenses/'+data.license, 20, 100, {width: 300, keepAspectRatio: true})
        //     .endPage()
        // }
        // pdfDoc
        // end and save
        .endPDF(() => {
            console.log('Done');
        })
        ;
        return path.join(__dirname, './contracts/'+data.n+'-'+data.date+'.pdf')
}

// editPdf(3,
//     {n:12,date:'20.05.19',sum:299000, entity:'I am Farkhaas ffad',
//     broadcast_type: [
//         {name: 'TYPE', q:12134},
//         {name: 'TYPE2', q:23, types: [
//         {name: 'TYPE', q:12134},
//         {name: 'TYPE', q:12134},
//         {name: 'TYPE', q:12134},
//         ]}
//     ]},
//     {
//         address: 'Yunusabad 12 sm. Ovoz',
//         address2: 'Yunusabad 12 sm. Ovoz',
//         phone:'+998909251019',
//         account: 2923993,
//         bank: ' ',
//         mfo: 2001,
//         inn: 12414235235
//     })
module.exports = editPdf;
// editPdf()у